<?php

namespace FileUploadBundle\EventListener\ORM;

use FileUploadBundle\ClassUtils;
use FileUploadBundle\Services\FileUploader;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class FileUploaderSubscriber implements EventSubscriber
{
    /** @var FileUploader */
    private $uploader;

    /**
     * FileUploaderSubscriber constructor.
     * @param FileUploader $uploader
     */
    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function getSubscribedEvents()
    {
        return [
            'loadClassMetadata',
            'prePersist',
            'preUpdate',
            'postLoad',
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $classMetadata = $eventArgs->getClassMetadata();
        $this->uploader->addConfiguration($classMetadata->getName());
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $class = get_class($entity);

        if ($this->uploader->hasConfiguration($class)) {
            $this->uploader->uploadAllProperties($entity);
        }
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $class = ClassUtils::getClass($entity);

        if ($this->uploader->hasConfiguration($class)) {
            $this->uploader->loadAllProperties($entity);
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $class = get_class($entity);

        if ($this->uploader->hasConfiguration($class)) {
            $this->uploader->updateAllProperties($entity);
        }
    }
}