<?php

namespace FileUploadBundle\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use FileUploadBundle\File\PublicFile;

class FileType extends Type
{
    /**
     * @inheritdoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'file';
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var PublicFile $value */
        return $value ? $value->getName() : null;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value ? new PublicFile($value) : null;
    }
}