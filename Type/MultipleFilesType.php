<?php

namespace FileUploadBundle\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use FileUploadBundle\File\PublicFile;
use Symfony\Component\HttpFoundation\File\File;

class MultipleFilesType extends Type
{
    /**
     * @inheritdoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'multiple_files';
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (! $value) {
            return null;
        }

        if (! is_array($value)) {
            throw new \Exception('Not array in multiple_files type');
        }

        $value = array_filter($value);

        if (! $value) {
            return null;
        }

        /**
         * Shold be interface in callback function argument type hint
         * @see Services/FileUploader.php:206 */
        $value = array_map(function(File $v){ return $v->getFilename(); }, $value);

        return serialize($value);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (! $value) {
            return null;
        }

        $value = unserialize($value);

        return array_map(function($v){ return new PublicFile($v); }, $value);
    }
}