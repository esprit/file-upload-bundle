<?php

namespace FileUploadBundle;

class ClassUtils
{
    public static function getClass($object)
    {
        if (class_exists(\Doctrine\Common\Util\ClassUtils::class)) {
            return \Doctrine\Common\Util\ClassUtils::getClass($object);
        }

        // TODO use ocramius/proxy-manager
        // https://www.doctrine-project.org/2018/07/12/common-2-9-and-dbal-2-8-and-orm-2-6-2.html

        return get_class($object);
    }
}