<?php

namespace FileUploadBundle\NameResolver;

use Symfony\Component\HttpFoundation\File\File;

interface NameResolverInterface
{
    public function generateFileName(File $file) : string ;
}