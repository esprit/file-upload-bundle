<?php

namespace FileUploadBundle\NameResolver;

use Symfony\Component\HttpFoundation\File\File;

class UniqueNameResolver implements NameResolverInterface
{
    public function generateFileName(File $file) : string
    {
        return md5(uniqid()) . '.' . $file->guessExtension();
    }
}