<?php

namespace FileUploadBundle;

use Doctrine\DBAL\Types\Type;
use FileUploadBundle\DependencyInjection\Compiler\NameResolverPass;
use FileUploadBundle\Type\FileType;
use FileUploadBundle\Type\MultipleFilesType;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FileUploadBundle extends Bundle
{
    public function boot()
    {
        // cause the error in console
        // [Doctrine\DBAL\DBALException]
        // Type file already exists.

        //Type::addType('file', FileType::class);
        //Type::addType('multiple_files', MultipleFilesType::class);


    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new NameResolverPass());
    }
}
