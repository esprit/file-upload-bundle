<?php

namespace FileUploadBundle\File;

class PublicFile extends BaseFile
{
    /** TODO For BC only */
    public function getSrc(): string
    {
        return $this->getPathname();
    }
}