<?php

namespace FileUploadBundle\File;

use FileUploadBundle\Annotations\FileUpload;

class BaseFile extends AbstractFile
{
    /** @var string */
    protected $filename;

    public function __construct(string $filename, FileUpload $config = null)
    {
        $this->filename = $filename;
        $this->config = $config;
    }

    public function getName(): string
    {
        return $this->filename;
    }
}