<?php

namespace FileUploadBundle\File;

use FileUploadBundle\Annotations\FileUpload;

interface FileUploadInterface
{
    public function setConfig(FileUpload $config);

    public function getPath() : string;

    public function getName() : string;
}