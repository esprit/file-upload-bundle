<?php

namespace FileUploadBundle\File;

use FileUploadBundle\Annotations\FileUpload;

abstract class AbstractFile implements FileUploadInterface
{
    /** @var FileUpload */
    protected $config;

    public function setConfig(FileUpload $config)
    {
        $this->config = $config;

        return $this;
    }

    public function getConfig() : FileUpload
    {
        return $this->config;
    }

    protected function isConfigured(): bool
    {
        return $this->config !== null;
    }

    protected function assertIfConfigured()
    {
        if (! $this->isConfigured()) {
            throw new \Exception(sprintf('Configuration for %s is not loaded.', get_class($this)));
        }
    }

    /**
     * Should be implemented for symfony validator
     * https://github.com/symfony/symfony/blob/c0ca2afbb3a395aadf1f9e2bb8f430c14899534f/src/Symfony/Component/Validator/Constraints/FileValidator.php#L120
     */
    public function __toString() : string
    {
        return $this->getPathname();
    }

    public function getPathname() : string
    {
        return implode(DIRECTORY_SEPARATOR, [
            $this->getPath(),
            $this->getName(),
        ]);
    }

    public function getPath(): string
    {
        $this->assertIfConfigured();

        return trim($this->config->value, DIRECTORY_SEPARATOR);
    }
}