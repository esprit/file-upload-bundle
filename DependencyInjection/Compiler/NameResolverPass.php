<?php

namespace FileUploadBundle\DependencyInjection\Compiler;

use FileUploadBundle\Services\FileUploader;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class NameResolverPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(FileUploader::class)) {
            return;
        }

        $definition = $container->findDefinition(FileUploader::class);
        $taggedServices = $container->findTaggedServiceIds('file_upload.name_resolver');

        foreach ($taggedServices as $id => $tags) {
            // a service could have the same tag twice
            foreach ($tags as $attributes) {
                $definition->addMethodCall('addNameResolver', array(
                    $attributes["alias"],
                    new Reference($id)
                ));
            }
        }
    }
}