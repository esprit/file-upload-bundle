<?php

namespace FileUploadBundle\Storage;

use FileUploadBundle\File\AbstractFile;
use Symfony\Component\HttpFoundation\File\File;

interface StorageInterface
{
    public function upload(File $uploadedFile, AbstractFile $file);
}