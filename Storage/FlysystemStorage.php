<?php

namespace FileUploadBundle\Storage;

use FileUploadBundle\File\AbstractFile;
use League\Flysystem\MountManager;
use Symfony\Component\HttpFoundation\File\File;

class FlysystemStorage implements StorageInterface
{
    /** @var MountManager */
    private $mountManager;

    /**
     * FlysystemStorage constructor.
     * @param MountManager $mountManager
     */
    public function __construct(MountManager $mountManager)
    {
        $this->mountManager = $mountManager;
    }

    public function upload(File $uploadedFile, AbstractFile $file): bool
    {
        $filesystem = $this->mountManager->getFilesystem($file->getConfig()->filesystemPrefix);

        $stream = fopen($uploadedFile->getRealPath(), 'r');
        $success = $filesystem->putStream($file->getPathname(), $stream);

        if (is_resource($stream)) {
            fclose($stream);
        }

        unlink($uploadedFile->getRealPath());

        return $success;
    }
}