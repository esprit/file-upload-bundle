<?php

namespace FileUploadBundle\Storage;

use FileUploadBundle\File\AbstractFile;
use Symfony\Component\HttpFoundation\File\File;

class FilesystemStorage implements StorageInterface
{
    public function upload(File $uploadedFile, AbstractFile $file)
    {
        $config = $file->getConfig();
        $baseDir = $config->dir;
        $absolutePath = $baseDir . DIRECTORY_SEPARATOR . $file->getPath();

        return $uploadedFile->move($absolutePath, $file->getName());
    }
}