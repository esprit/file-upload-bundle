<?php

namespace FileUploadBundle\Form\Type;

use FileUploadBundle\File\PublicFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Options;

class FileUploadType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['multiple'] = $options['multiple'] ?? false;
    }

    public function getBlockPrefix()
    {
        return 'file_upload';
    }

    public function getParent()
    {
        return FileType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('data_class', function (Options $options) {
            return $options['multiple'] ? null : PublicFile::class;
        });

    }
}