<?php

namespace FileUploadBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Options;
use FileUploadBundle\File\PublicFile;

class ImageUploadType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['multiple'] = $options['multiple'] ?? false;
    }

    public function getBlockPrefix()
    {
        return 'image_upload';
    }

    public function getParent()
    {
        return FileType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', function (Options $options) {
            return $options['multiple'] ? null : PublicFile::class;
        });

    }

}