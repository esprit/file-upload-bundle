<?php

namespace FileUploadBundle\Services;

use FileUploadBundle\Annotations\FileUpload;
use Doctrine\Common\Annotations\AnnotationReader;
use FileUploadBundle\ClassUtils;
use FileUploadBundle\File\AbstractFile;
use FileUploadBundle\File\FileUploadInterface;
use FileUploadBundle\File\PublicFile;
use FileUploadBundle\NameResolver\NameResolverInterface;
use FileUploadBundle\Storage\StorageInterface;
use League\Flysystem\FileNotFoundException;
use ReflectionException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    /**
     * Global configuration
     * @var array
     */
    private $globalConfiguration;

    /**
     * @var array
     */
    private $configuration;

    /** @var NameResolverInterface[] */
    private $nameResolvers;

    /** @var StorageInterface */
    private $storage;

    /**
     * FileUploader constructor.
     * @param $config
     */
    public function __construct(StorageInterface $storage, array $globalConfiguration)
    {
        $this->storage = $storage;
        $this->globalConfiguration = $globalConfiguration;
    }

    public function addNameResolver(string $alias, NameResolverInterface $nameResolver)
    {
        if (isset($this->nameResolvers[$alias])) {
            throw new \Exception(sprintf('Name resolver with alias %s already exists', $alias));
        }

        $this->nameResolvers[$alias] = $nameResolver;
    }

    private function getNameResolver(string $alias) : NameResolverInterface
    {
        return $this->nameResolvers[$alias];
    }

    public function mergeWithGlobals(FileUpload $config)
    {
        $config->dir = $config->dir ?: $this->globalConfiguration['dir'];
        $config->url = $config->url ?: $this->globalConfiguration['url'];

        return $config;
    }

    public function addConfiguration(string $class, $property = null)
    {
        if (null === $property) {
            $reflectionClass = new \ReflectionClass($class);

            foreach ($reflectionClass->getProperties() as $reflectionProperty) {
                $property = $reflectionProperty->getName();

                $this->addConfiguration($class, $property);
            }
            return;
        }

        if ($this->hasConfiguration($class, $property)) {
            return;
        }

        $reader = new AnnotationReader();
        try {
            $reflectionProperty = new \ReflectionProperty($class, $property);
        }catch (ReflectionException $e) {
            $reflectionClass = new \ReflectionClass($class);
            $parentClass = $reflectionClass->getParentClass()->getName();
            $reflectionProperty = new \ReflectionProperty($parentClass, $property);
        }

        /** @var FileUpload $annotation */
        $annotation = $reader->getPropertyAnnotation($reflectionProperty, FileUpload::class);

        if ($annotation) {
            $this->configuration[$class][$property] = $this->mergeWithGlobals($annotation);
        }
    }

    /**
     * @param $entity
     * @param $property
     * @return mixed
     */
    public function getConfiguration($entity, $property = null)
    {
        $class = ClassUtils::getClass($entity);

        if (null === $property) {
            return $this->configuration[$class] ?? [];
        }

        if (! $this->hasConfiguration($class, $property)) {
            $this->addConfiguration($class, $property);
        }

        if (! $this->hasConfiguration($class, $property)) {
            throw new \Exception(sprintf('Cant load confifuration for %s %s', $class, $property));
        }

        return $this->configuration[$class][$property] ?? [];
    }

    public function hasConfiguration(string $class, $property = null)
    {
        if (null === $property) {
            return isset($this->configuration[$class]);
        }

        return isset($this->configuration[$class][$property]);
    }

    public function getPropertyValue($entity, $property)
    {
        $closure = \Closure::bind(function ($entity) use ($property) {
            return $entity->{$property};
        }, null, get_class($entity));

        return $closure($entity);
    }

    public function uploadMultipleFiles(array $files, FileUpload $config)
    {
        return array_filter(array_map(function ($f) use ($config) {
            return $this->uploadFile($f, $config);
        }, $files));
    }

    public function uploadFile($uploadedFile, FileUpload $config)
    {
//        if (is_string($file)) {
//            return $file;
//        }
//        if (! $file instanceof File) {
//            return null;
//        }

        if ($uploadedFile instanceof FileUploadInterface) {
            // allow pre-loaded instance
            return $uploadedFile;
        }

        // TODO the uploaded file should be the instance of FileUploadedInterface
        // It is not good idea to change instance type of (doctrine entity/object) field
        if (! $uploadedFile instanceof File) {
            throw new \Exception('Only instance of %s allowed for upload', File::class);
        }

        /** @var UploadedFile $uploadedFile */
        $nameResolver = $this->getNameResolver($config->nameResolver);
        $fileName = $nameResolver->generateFileName($uploadedFile);

        /** @var AbstractFile $file */
        $file = $config->createFileInstance($fileName);

        if ($config->filesystemPrefix) {
            $this->storage->upload($uploadedFile, $file);
        }

        return $file;
    }

    public function uploadAllProperties($entity)
    {
        foreach ($this->getConfiguration($entity) as $property => $config) {
            $this->uploadProperty($entity, $property);
        }
    }

    public function uploadProperty($entity, $property)
    {
        $value = $this->getPropertyValue($entity, $property);

        /** @var FileUpload $config */
        $config = $this->getConfiguration($entity, $property);

        if (!$value) {
            return;
        }

        $value = $config->multiple
            ? $this->uploadMultipleFiles($value, $this->getConfiguration($entity, $property))
            : $this->uploadFile($value, $this->getConfiguration($entity, $property));

        // после загрузки получаем обьект (список обьектов) типа Symfony\Component\HttpFoundation\File\File
        // TODO convert File to PublicFile, or another solution

        $this->setPropertyValue($entity, $property, $value);
    }

    public function loadMultipleFiles(array $fileNames, FileUpload $config)
    {
        return array_filter(array_map(function ($f) use ($config) {
            return $this->loadFile($f, $config);
        }, $fileNames));
    }

    /**
     * Load file from string
     *
     * @param            $fileName
     * @param FileUpload $config
     * @return string|FileUploadInterface
     */
    public function loadFile($fileName, FileUpload $config)
    {
        if (null === $fileName) {
            return null;
        }

        if (! $config->load) {
            return $fileName;
        }

        if (! $fileName instanceof FileUploadInterface) {
            throw new \Exception('Cant set config');
        }

        /** @var PublicFile $fileName */
        $fileName->setConfig($config);

        return $fileName;
    }

    public function loadAllProperties($entity)
    {
        foreach ($this->getConfiguration($entity) as $property => $config) {
            $this->loadProperty($entity, $property);
        }
    }

    /**
     * Load File from string in property
     *
     * @param       $entity
     * @param       $property
     * @param array $config
     */
    public function loadProperty($entity, $property = null)
    {
        $value = $this->getPropertyValue($entity, $property);

        if (! $value) {
            return;
        }

        // доктрина уже отдает нам нужный тип (PublicFile) с указанным именем файла
        // всё что нужно сделать это установить конфигурацию, чтобы знать пути к файлу

        /** @var FileUpload $config */
        $config = $this->getConfiguration($entity, $property);
        $value = $config->multiple
            ? $this->loadMultipleFiles($value, $config)
            : $this->loadFile($value, $config);

        $this->setPropertyValue($entity, $property, $value);
    }

    public function updateAllProperties($entity)
    {
        foreach ($this->getConfiguration($entity) as $property => $config) {
            $this->updateProperty($entity, $property);
        }
    }

    public function updateProperty($entity, $property)
    {
        $value = $this->getPropertyValue($entity, $property);

        if (! $value) {
            return;
        }

        /** @var FileUpload $config */
        $config = $this->getConfiguration($entity, $property);
        $value = $config->multiple
            ? $this->updateMultipleFiles($value, $config)
            : $this->updateFile($value, $config);

        $this->setPropertyValue($entity, $property, $value);
    }

    public function updateMultipleFiles($files, FileUpload $config)
    {
        return array_filter(array_map(function ($f) use ($config) {
            return $this->updateFile($f, $config);
        }, $files));
    }

    public function updateFile($file, FileUpload $config)
    {
        if ($file instanceof UploadedFile) {
            return $this->uploadFile($file, $config);
        }

        if ($config->load and ! ($file instanceof FileUploadInterface)) {
            throw new \Exception('Incorrect value type');
        }

        return $file;
    }

    private function setPropertyValue($entity, $property, $value)
    {
        $closure = \Closure::bind(function ($entity) use ($property, $value) {
            $entity->{$property} = $value;
        }, null, get_class($entity));

        $closure($entity);
    }
}