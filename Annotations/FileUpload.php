<?php

namespace FileUploadBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;
use FileUploadBundle\File\AbstractFile;
use FileUploadBundle\File\FileUploadInterface;
use FileUploadBundle\File\PublicFile;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
final class FileUpload extends Annotation
{
    /** @var string */
    public $dir;

    /** @var string */
    public $url;

    /** @var boolean */
    public $load = true;

    /** @var bool */
    public $multiple = false;

    /** @var string */
    public $nameResolver = 'unique';

    /** @var string */
    public $pathResolver;

    /** @var string */
    public $class = PublicFile::class;

    /** @var string */
    public $factoryMethod;

    /** @var string */
    public $filesystemPrefix;

    public function createFileInstance(string $filename): AbstractFile
    {
        $class = $this->class;

        if ($this->factoryMethod) {
            return call_user_func([$class, $this->factoryMethod], $filename, $this);
        }

        return new $class($filename, $this);
    }
}