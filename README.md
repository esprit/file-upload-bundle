# FileUploadBundle #

The FileUploadBundle is a Symfony bundle that attempts to ease file uploads that are attached to ORM entities, 

Usage example

```php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FileUploadBundle\Annotations\FileUpload;
use FileUploadBundle\File\PublicFile;

/**
 * @ORM\Entity()
 */
class Request
{
    //---
    
    /**
     * @var PublicFile[]|null
     * @ORM\Column(type="multiple_files", nullable=true)
     * @FileUpload("request/files", multiple=true)
     */
    private $files;

    /**
     * @return PublicFile[]|null
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param array|null $files
     */
    public function setFiles(array $files = null)
    {
        // если меняли другие поля (с формы для поля приходит null),
        // в этом случае прежние файлы должны остаться
        if (! $files) {
            return;
        }

        $this->files = $files;
    }
}
```

```php

class RequestType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //...
            ->add('files', FileUploadType::class, ['multiple' => true, 'required' => false])
        ;
    }
}
```

Changelog
=========

Current dev

- Add Flysystem storage support
- [BC break] FileUploadInterface::getPath now return a relative path

- refactoring in File dir
- [BC break] remove getWidth and getHeight from PublicFile
- remove __toString & create from FileUploadedInterface, it is implemented in AbstractFile

version 1.2

- [BC break] remove deprecated upload_file twig helper
- [BC break] PublicFile not extends File
- [BC break] rename PublicFile.path to PublicFile.path (to get full path use PublicFile.pathname)
- allow configure custom file class

version 1.1

- no need setter anymore